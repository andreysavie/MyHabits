# MyHabits (Мои привычки)

[![Swift 5](https://img.shields.io/badge/Swift-5.0-orange.svg?style=flat)](https://swift.org)
[![Telegram: @andreysavie](https://img.shields.io/badge/Contact-Telegram-blue.svg?style=flat)](https://t.me/andreysavie)


Приложение MyHabits было разработано в рамках учебного процесса на курсе [Нетологии](https://netology.ru/)


<h3 align="center">
<img src="Screenshots/Promo.png" alt="Promo of "MyHabits" project" />
</h3>

## Про приложение

MyHabits помогает Вам следить обретать новые привычки и следить за их выполнением. Главный экран содержит все добавленные пользователем привычки с отметкой трекинга.

<h3>
<img src="Screenshots/HabitsVC.png" alt="Screenshot of main screen" width="250" align="left"/>
<img src="Screenshots/HabitVC.png" alt="Screenshot of Habit screen" width="250" align="center"/>
<img src="Screenshots/DetailsVC.png" alt="Screenshot of Details screen" width="250" align="right"/>
</h3>


## Совместимость

Этот проект написан на Swift версии 5.6.1 и доступен для Xcode версии 13 и более поздних (поддержка iOS минимальной версии 15.2)

## Про код

Проект построен исключительно нативнными инструментами. При написании я старался придерживаться принципов ООП и Solid. Вёрстка интерфейса осуществлена с помощью UIKit на основе дизайн-проекта. Вёрстка адаптивная, с использованием AutoLayout, интерфейс правильно отображается на всех моделях мобильных телефонов Apple.


## Автор

* [Андрей Рыбалкин](https://github.com/andreysavie)

